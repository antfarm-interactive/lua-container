FROM alpine:latest

ARG LUA_VERSION
ARG LUA_ROCKS_VERSION

RUN apk add make gcc musl-dev openssl wget unzip

RUN wget http://www.lua.org/ftp/lua-${LUA_VERSION}.tar.gz && \
    tar xf lua-${LUA_VERSION}.tar.gz && \
    cd lua-${LUA_VERSION} && \
    make all test && \
    ln -s /lua-${LUA_VERSION}/src/lua /usr/bin/lua

RUN wget https://luarocks.org/releases/luarocks-${LUA_ROCKS_VERSION}.tar.gz && \
    tar zxpf luarocks-${LUA_ROCKS_VERSION}.tar.gz && \
    cd luarocks-${LUA_ROCKS_VERSION} && \
    ./configure --with-lua-include=/lua-${LUA_VERSION}/src/ && \
    make bootstrap

CMD ["lua", "-v"]
